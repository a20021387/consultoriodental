﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class EstatusPersonalValidator:AbstractValidator<EstatusPersonal>
    {
        public EstatusPersonalValidator()
        {
            RuleFor(e => e.Estatus).NotEmpty().MaximumLength(50);
        }
    }
}
