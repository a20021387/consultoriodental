﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class RegistroDeEventoValidator:AbstractValidator<RegistroDeEvento>
    {
        public RegistroDeEventoValidator()
        {
            RuleFor(r => r.FechaHora).NotEmpty();
            RuleFor(r=>r.CURP).NotEmpty().MaximumLength(50);
            RuleFor(r=>r.Descripcion).NotEmpty().MaximumLength(5000);
            RuleFor(r=>r.ConsultaSQL).NotEmpty().MaximumLength(5000);
            RuleFor(r=>r.Tabla).NotEmpty().MaximumLength(50);
        }
    }
}
