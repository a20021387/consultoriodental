﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class CitasValidator:AbstractValidator<Cita>
    {
        public CitasValidator()
        {
            RuleFor(c => c.IdDoctor).NotEmpty();
            RuleFor(c=>c.IdPaciente).NotEmpty();
            //NOTA: los bool no se validan
            RuleFor(c=>c.FechaHora).NotEmpty();
            RuleFor(c => c.Procedimiento).NotEmpty().MaximumLength(5000);
            RuleFor(c => c.Observaciones).MaximumLength(500);
            RuleFor(c=>c.Tratamiento).NotEmpty().MaximumLength(500);
            RuleFor(c => c.RetroalimentacionDelPaciente).MaximumLength(2500);

        }
    }
}
