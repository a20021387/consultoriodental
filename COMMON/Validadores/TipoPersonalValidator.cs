﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class TipoPersonalValidator:AbstractValidator<TipoPersonal>
    {
        public TipoPersonalValidator()
        {
            RuleFor(t => t.Rol).NotEmpty().MaximumLength(50);
        }
    }
}
