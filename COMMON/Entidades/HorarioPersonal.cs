﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class HorarioPersonal
    {
        public int IdHorarioPersonal { get; set; }
        public int IdPersonal { get; set; }
        public int NumDiaSemana { get; set; }
        public DateTime HoraEntrada { get; set; }
        public DateTime HoraSalida { get; set; }
    }
}

