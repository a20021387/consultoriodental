﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Paciente:Persona
    {
        public int IdPaciente { get; set; }
        public string Domicilio { get; set; }
        public string Alergias { get; set; }
        public string TelEmergencia { get; set; }
        public string ContactoDeEmergencia { get; set; }
        public string AntecedentesMedicos { get; set; }
    }
}
