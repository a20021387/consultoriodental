﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public abstract class Persona
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public char Genero { get; set; }
        public string CURP { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
