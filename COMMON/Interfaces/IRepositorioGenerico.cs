﻿namespace COMMON.Interfaces
{
    public interface IRepositorioGenerico<T> where T : class
    {
        string CampoId { get; }
        string Error { get; }
        IEnumerable<T> Get { get; }
        bool IdEsAutonumerico { get; }
        string NombreTabla { get; }

        bool Delete(string id);
        T GetById(string id);
        T Insert(T item, string id = "");
        T Update(T item, string id);
    }
}