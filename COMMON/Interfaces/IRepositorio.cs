﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Interfaces
{
    public interface IRepositorio<T> where T : class
    {
        /// <summary>
        /// Indica el error (si es que se tiene) al ejecutar alguna operación sobre la BD.
        /// </summary>
        string Error { get; }

        /// <summary>
        /// Obtiene todos los elementos de la tabla
        /// </summary>
        IEnumerable<T> Get { get; }
        /// <summary>
        /// Obtiene el elemento con el Id especificado
        /// </summary>
        /// <param name="id">Id del elemento a obtener</param>
        /// <returns>Registro</returns>
        T GetById(int id);
        /// <summary>
        /// Inserta un elemento en la BD
        /// </summary>
        /// <param name="item">Elemento a Insertar</param>
        /// <returns>Objeto insertado completo</returns>
        T Insert(T item);
        /// <summary>
        /// Actualiza un registro en base al Id
        /// </summary>
        /// <param name="item">Registro a actualizar</param>
        /// <param name="id">Id del registro a actualizar</param>
        /// <returns>Registro actualizado</returns>
        T Update(T item,int id);
        /// <summary>
        /// Elimina un registro en base a su Id
        /// </summary>
        /// <param name="id">Id del registro a eliminar</param>
        /// <returns>Booleano indicando si pudo ser eliminado</returns>
        bool Delete(int id);
    }
}
