﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public abstract class GenericManager<T> where T : class
    {
        protected string Tabla { get; private set; }
        protected string UrlBase { get; private set; }
        protected HttpClient HttpClient { get; private set; }
        public string Error { get; private set; }
        public GenericManager(string urlBase,string tabla)
        {
            Tabla = tabla;
            UrlBase = urlBase;
            Error = "";
            HttpClient=new HttpClient();
            HttpClient.BaseAddress = new Uri(UrlBase);
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
        #region MetodoCRUD
        public List<T> ObtenerTodos
        {
            get
            {
                return ObtenerTodosAsync().Result;
            }
        }

        public T Insertar(T item)
        {
           return InsertarAsync(item).Result;
        }

        public T Actualizar(T item,int id)
        {
            return ActualiarAsync(item,id).Result;
        }

        public bool Eliminar(int id)
        {
            return EliminarAsync(id).Result;
        }

        #endregion

        #region MetodosAsincronos
        public async Task<List<T>> ObtenerTodosAsync()
        {
            HttpResponseMessage response=await HttpClient.GetAsync($"{UrlBase}/api/{Tabla}").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if(response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<T>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

        public async Task<T> InsertarAsync(T item)
        {
            var c=JsonConvert.SerializeObject(item);
            var body=new StringContent (c, Encoding.UTF8, "application/json");
            HttpResponseMessage response=await HttpClient.PostAsync($"{UrlBase}/api/{Tabla}",body).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

        public async Task<T> ActualiarAsync(T item,int id)
        {
            var c = JsonConvert.SerializeObject(item);
            var body = new StringContent(c, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await HttpClient.PutAsync($"{UrlBase}/api/{Tabla}/{id}", body).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }

        public async Task<bool> EliminarAsync(int id)
        {
            HttpResponseMessage response = await HttpClient.DeleteAsync($"{UrlBase}/api/{Tabla}/{id}").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return true;
            }
            else
            {
                Error = content;
                return false;
            }
        }



        #endregion

    }
}
