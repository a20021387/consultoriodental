﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class FabricManager
    {
        string UrlBase;
        public FabricManager(string urlBase)
        {
            urlBase = urlBase;
        }

        public CitasManager CitasManager() => new CitasManager(UrlBase, "Citas");
        public EstatusPersonalManager EstatusPersonalManager() => new EstatusPersonalManager(UrlBase, "EstatusPersonal");
        //TODO: Terminar los demas managers

    }
}
