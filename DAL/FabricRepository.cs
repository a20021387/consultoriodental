﻿using COMMON.Entidades;
using COMMON.Interfaces;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class FabricRepository
    {
        string cadenaDeConexion;
        public FabricRepository(string cadenaDeConexion)
        {
            this.cadenaDeConexion = cadenaDeConexion;
        }

        public object ObtenerRepositorio(string clase)
        {
            switch (clase)
            {
                case "Cita":
                    return new RepositorioGenerico<Cita>(cadenaDeConexion, new CitasValidator(), "Citas", "IdCita");
                case "EstatusPersonal":
                    return new RepositorioGenerico<EstatusPersonal>(cadenaDeConexion, new EstatusPersonalValidator(), "EstatusPersonal", "IdEstatus");
                case "Paciente":
                    return new RepositorioGenerico<Paciente>(cadenaDeConexion, new PacienteValidador(), "Pacientes", "IdPaciente");
                case "Personal":
                    return new RepositorioGenerico<Personal>(cadenaDeConexion, new PersonalValidator(), "Personal", "IdPersonal");
                case "RegistroDeEvento":
                   return new RepositorioGenerico<RegistroDeEvento>(cadenaDeConexion, new RegistroDeEventoValidator(), "RegistroDeEventos", "IdRegistro");
                case "TipoPersonal":
                    return new RepositorioGenerico<TipoPersonal>(cadenaDeConexion, new TipoPersonalValidator(), "TipoPersonal", "IdTipoPersonal");
                default:
                    return null;
            }
        }
        public IRepositorioGenerico<Cita> RepositorioCitas() => new RepositorioGenerico<Cita>(cadenaDeConexion, new CitasValidator(), "Citas", "IdCita");

        public IRepositorioGenerico<EstatusPersonal> RepositorioEstatusPersonal() => new RepositorioGenerico<EstatusPersonal>(cadenaDeConexion, new EstatusPersonalValidator(), "EstatusPersonal", "IdEstatus");

        public IRepositorioGenerico<Paciente> RepositorioPacientes() => new RepositorioGenerico<Paciente>(cadenaDeConexion, new PacienteValidador(), "Pacientes", "IdPaciente");

        public IRepositorioGenerico<Personal> RepositorioPersonal() => new RepositorioGenerico<Personal>(cadenaDeConexion, new PersonalValidator(), "Personal", "IdPersonal");

        public IRepositorioGenerico<RegistroDeEvento> RepositorioRegistroDeEvento() => new RepositorioGenerico<RegistroDeEvento>(cadenaDeConexion, new RegistroDeEventoValidator(), "RegistroDeEventos", "IdRegistro");

        public IRepositorioGenerico<TipoPersonal> RepositorioTipoPersonal() => new RepositorioGenerico<TipoPersonal>(cadenaDeConexion, new TipoPersonalValidator(), "TipoPersonal", "IdTipoPersonal");

    }
}
