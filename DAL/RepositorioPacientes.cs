﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class RepositorioPacientes : IRepositorio<Paciente>
    {
        SqlConnection conexion;
        public RepositorioPacientes(string cadConexion)
        {
            conexion=new SqlConnection(cadConexion);

        }
        public string Error{get; private set;}

        public IEnumerable<Paciente> Get
        {
            get
            {
                try
                {
                    List<Paciente> list = new List<Paciente>();
                    conexion.Open();
                    SqlCommand cmd = new SqlCommand("Select * from Pacientes", conexion);
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Paciente paciente = new Paciente();
                        paciente.IdPaciente = dataReader.GetInt32(0);
                        paciente.Nombre = dataReader.GetString(1);
                        paciente.Apellidos = dataReader.GetString("Apellidos");
                        paciente.Genero = dataReader.GetChar("Genero");
                        paciente.CURP = dataReader.GetString("CURP");
                        paciente.Telefono = dataReader.GetString("Telefono");
                        paciente.Email = dataReader.GetString("Email");
                        paciente.Password = dataReader.GetString("Password");
                        paciente.Domicilio = dataReader.GetString("Domicilio");
                        paciente.Alergias = dataReader.GetString("Alergias");
                        paciente.TelEmergencia = dataReader.GetString("TelEmergencias");
                        paciente.ContactoDeEmergencia = dataReader.GetString("ContactoDeEmergencia");
                        paciente.AntecedentesMedicos = dataReader.GetString("AntecedentesMedicos");
                        list.Add(paciente);

                    }
                    dataReader.Close();
                    conexion.Close();
                    Error = "";
                    return list;
                }
                catch (Exception ex)
                {
                    if (conexion.State == System.Data.ConnectionState.Open)
                    {
                        conexion.Close();
                    }
                    Error = ex.Message;
                    return null;
                }
                

            }
        }

        public bool Delete(int id)
        {
            try
            {
                conexion.Open();
                string sql = $"delete from Pacientes where IdPaciente={id}";
                SqlCommand cmd = new SqlCommand(sql, conexion);
                int r = cmd.ExecuteNonQuery();
                conexion.Close();
                if (r > 0)
                {
                    Error = "";
                    return true;
                }
                else
                {
                    Error = "No existe dicho elemento";
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return false;
            }
        }

        public Paciente GetById(int id)
        {
            try
            {
                List<Paciente> list = new List<Paciente>();
                conexion.Open();
                SqlCommand cmd = new SqlCommand($"Select * from Pacientes where IdPaciente={id}", conexion);
                SqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    Paciente paciente = new Paciente();
                    paciente.IdPaciente = dataReader.GetInt32(0);
                    paciente.Nombre = dataReader.GetString(1);
                    paciente.Apellidos = dataReader.GetString("Apellidos");
                    paciente.Genero = dataReader.GetChar("Genero");
                    paciente.CURP = dataReader.GetString("CURP");
                    paciente.Telefono = dataReader.GetString("Telefono");
                    paciente.Email = dataReader.GetString("Email");
                    paciente.Password = dataReader.GetString("Password");
                    paciente.Domicilio = dataReader.GetString("Domicilio");
                    paciente.Alergias = dataReader.GetString("Alergias");
                    paciente.TelEmergencia = dataReader.GetString("TelEmergencias");
                    paciente.ContactoDeEmergencia = dataReader.GetString("ContactoDeEmergencia");
                    paciente.AntecedentesMedicos = dataReader.GetString("AntecedentesMedicos");
                    list.Add(paciente);

                }
                dataReader.Close();
                conexion.Close();
                Error = "";
                return list.SingleOrDefault();
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }

        public Paciente Insert(Paciente item)
        {
            try
            {
                //TODO: Completar este insert

                //29/03/2023 11:54:30 am --> '2023/03/29 11:54:30'
                //string fecha = $"{item.FechaHora.Year}/{item.FechaHora.Month}/{item.FechaHora.Day} {item.FechaHora.Hour}:{item.FechaHora.Minute}:{item.FechaHora.Second}";
                //string fecha2 = item.FechaHora.ToString("yyyy/MM/dd HH:mm:ss");


                //string sql = $"insert into productos (Id,FechaHora,IdCategoria,Nombre,Precio) values('{item.Id}','{fecha}','{item.IdCategoria}','{item.Nombre}',{item.Precio})";
                //SqlCommand cmd = new SqlCommand(sql, conexion);
                //int r = cmd.ExecuteNonQuery();//insert, update, delete
                //conexion.Close();
                //if (r > 0)
                //{
                //    Error = "";
                //    return item;
                //}
                //else
                //{
                //    Error = "Error al insertar el registro...";
                //    return null;
                //}
                return null;
            }
            catch (Exception ex)
            {
                if (conexion.State == System.Data.ConnectionState.Open)
                {
                    conexion.Close();
                }
                Error = ex.Message;
                return null;
            }
        }

        public Paciente Update(Paciente item, int id)
        {
            //TODO: Falta por completar este UPDATE
            throw new NotImplementedException();
        }
    }
}
