﻿using COMMON.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System.Data;
using System.Reflection;
using System.Text;

namespace DAL
{
    public class RepositorioGenerico<T> : IRepositorioGenerico<T> where T : class
    {
        public string CampoId { get; private set; }
        public string NombreTabla { get; private set; }
        public bool IdEsAutonumerico { get; private set; }
        public string Error { get; private set; }

        private SqlConnection conexion;
        private AbstractValidator<T> validator;
        public RepositorioGenerico(string cadConexion, AbstractValidator<T> validador, string nombreTabla, string campoId, bool idEsNumerico = true)
        {
            conexion = new SqlConnection(cadConexion);
            CampoId = campoId;
            NombreTabla = nombreTabla;
            validator = validador;
            IdEsAutonumerico = idEsNumerico;
            Error = "";
        }
        #region CRUD
        public IEnumerable<T> Get
        {
            get
            {
                try
                {
                    var datos = ObtenerDatos<T>($"Select * from {NombreTabla} FOR JSON AUTO");
                    return datos != null ? datos : null;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public bool Delete(string id)
        {
            try
            {
                conexion.Open();
                SqlCommand cmd;
                if (IdEsAutonumerico)
                {
                    cmd = new SqlCommand($"Delete from {NombreTabla} Where {CampoId}={id}", conexion);
                }
                else
                {
                    cmd = new SqlCommand($"Delete from {NombreTabla} Where {CampoId}='{id}'", conexion);

                }
                int r = cmd.ExecuteNonQuery();
                CerrarConexion();
                Error = "";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                CerrarConexion();
                return false;
            }
        }

        public T GetById(string id)
        {
            try
            {
                List<T> datos = new List<T>();
                if (IdEsAutonumerico)
                {
                    datos = ObtenerDatos<T>($"Select * from {NombreTabla} where {CampoId}={id} for JSON AUTO");
                }
                else
                {
                    datos = ObtenerDatos<T>($"Select * from {NombreTabla} where {CampoId}='{id}' for JSON AUTO");
                }
                return datos != null ? datos.SingleOrDefault() : null;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public T Insert(T item, string id = "")
        {
            try
            {
                ValidationResult validationResult = validator.Validate(item);
                if (validationResult.IsValid)
                {
                    List<T> datos = new List<T>();
                    datos.Add(item);
                    DataTable table = ToDataTable<T>(datos);
                    conexion.Open();
                    int r = 0;
                    using (var cmd = new SqlCommand($"Select * from {NombreTabla}", conexion))
                    {
                        using (var da = new SqlDataAdapter(cmd))
                        {
                            using (var cb = new SqlCommandBuilder(da))
                            {
                                r = da.Update(table);
                            }
                        }
                    }
                    CerrarConexion();
                    Error = "";
                    if (IdEsAutonumerico)
                    {
                        return r == 1 ? ObtenerDatos<T>($"select top 1 * from {NombreTabla} order by {CampoId} desc FOR JSON AUTO").SingleOrDefault() : null;
                    }
                    else
                    {
                        return r == 1 ? GetById(id) : null;
                    }
                }
                else
                {
                    string errors = "Error de validación: ";
                    foreach (var error in validationResult.Errors)
                    {
                        errors += error.ErrorMessage + ". ";
                    }
                    Error = errors;
                    return null;
                }

            }
            catch (Exception ex)
            {
                CerrarConexion();
                Error = ex.Message;
                return null;
            }
        }

        public T Update(T item, string id)
        {
            try
            {
                ValidationResult validationResult = validator.Validate(item);
                if (validationResult.IsValid)
                {
                    List<T> datos = new List<T>();
                    datos.Add(item);
                    DataTable table = ToDataTable<T>(datos);
                    conexion.Open();
                    int r = 0;
                    SqlDataAdapter da;
                    if (IdEsAutonumerico)
                    {
                        da = new SqlDataAdapter($"Select * from {NombreTabla} where {CampoId}={id}", conexion);
                    }
                    else
                    {
                        da = new SqlDataAdapter($"Select * from {NombreTabla} where {CampoId}='{id}'", conexion);
                    }
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dt.Rows[0].BeginEdit();
                    foreach (DataColumn colum in dt.Columns)
                    {
                        dt.Rows[0][colum.ColumnName] = table.Rows[0][colum.ColumnName];
                    }
                    dt.Rows[0].EndEdit();
                    var cb = new SqlCommandBuilder(da);
                    da.AcceptChangesDuringUpdate = true;
                    r = da.Update(dt);
                    CerrarConexion();
                    Error = "";
                    return r == 1 ? GetById(id) : null;
                }
                else
                {
                    string errors = "Error de validación: ";
                    foreach (var error in validationResult.Errors)
                    {
                        errors += error.ErrorMessage + ". ";
                    }
                    Error = errors;
                    return null;
                }

            }
            catch (Exception ex)
            {
                CerrarConexion();
                Error = ex.Message;
                return null;
            }
        }

        #endregion

        #region MetodosDeApoyo

        protected DataTable ToDataTable<M>(List<M> items)
        {
            DataTable dataTable = new DataTable(typeof(M).Name);

            PropertyInfo[] Props = typeof(M).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (M item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
        protected List<M> ObtenerDatos<M>(string sql) where M : class
        {
            try
            {
                conexion.Open();
                StringBuilder json = new StringBuilder();
                if(!sql.ToLower().Contains("for json auto"))
                {
                    sql += " FOR JSON AUTO";
                }
                using (var cmd = new SqlCommand(sql, conexion))
                {
                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        json.Append("[]");
                    }
                    else
                    {
                        while (reader.Read())
                        {
                            json.Append(reader.GetString(0).ToString());
                        }
                    }
                }
                CerrarConexion();
                Error = "";
                return JsonConvert.DeserializeObject<List<M>>(json.ToString());
            }
            catch (Exception ex)
            {
                CerrarConexion();
                Error = ex.Message;
                return null;
            }
        }
        protected void CerrarConexion()
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }
        }
        #endregion
    }
}
