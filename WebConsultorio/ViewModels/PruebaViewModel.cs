﻿using COMMON.Entidades;
using Microsoft.AspNetCore.Components;

namespace WebConsultorio.ViewModels
{
    public class PruebaViewModel:ComponentBase 
    {
        protected DateTime ahora = DateTime.Now;
        protected EstatusPersonal estado = new EstatusPersonal()
        {
            Estatus = "Nuevo"
        };

        protected void CambiarEstatus()
        {
            estado.Estatus = "Activo";
        }
    }
}
