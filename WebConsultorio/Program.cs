using BIZ;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using WebConsultorio.Data;

namespace WebConsultorio
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            string url;
#if (DEBUG)
            url = builder.Configuration.GetValue<string>("URLDebug");
#else
            url = builder.Configuration.GetValue<string>("URLRelease");
#endif


            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();
            builder.Services.AddSingleton<WeatherForecastService>();
            builder.Services.AddSingleton(typeof(FabricManager),new FabricManager(url));
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }


            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}