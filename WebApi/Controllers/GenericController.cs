﻿using COMMON.Interfaces;
using DAL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericController<T> : ControllerBase where T : class
    {
        FabricRepository fabricRepository;
        IRepositorioGenerico<T> repo;
        public GenericController(FabricRepository fabric)
        {
            fabricRepository= fabric;
            repo = fabricRepository.ObtenerRepositorio(typeof(T).Name) as IRepositorioGenerico<T>;
        }


        //RESTFULL
        // GET: api/<GenericController>
        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            var datos = repo.Get;
            if(datos == null)
            {
                return BadRequest(repo.Error);
            }
            else
            {
                return Ok(datos);
            }
        }

        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(int id)
        {
            var dato = repo.GetById(id.ToString());
            if (dato == null)
            {
                return BadRequest(repo.Error);
            }
            else
            {
                return Ok(dato);
            }
        }

        // POST api/<GenericController>
        // POST=INSERT
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            var dato = repo.Insert(value);
            if (dato == null)
            {
                return BadRequest(repo.Error);
            }
            else
            {
                return Ok(dato);
            }

        }

        // PUT api/<GenericController>/5
        // PUT=UPDATE
        [HttpPut("{id}")]
        public ActionResult<T> Put(int id, [FromBody] T value)
        {
            var dato = repo.Update(value, id.ToString());
            if (dato == null)
            {
                return BadRequest(repo.Error);
            }
            else
            {
                return Ok(dato);
            }
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(int id)
        {
            var dato=repo.Delete(id.ToString());
            if (!string.IsNullOrEmpty(repo.Error))
            {
                return BadRequest(repo.Error);
            }
            else
            {
                return Ok(dato);
            }
        }
    }
}
