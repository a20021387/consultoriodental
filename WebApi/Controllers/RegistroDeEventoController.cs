﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroDeEventoController : GenericController<RegistroDeEvento>
    {
        public RegistroDeEventoController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
