﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalController : GenericController<Personal>
    {
        public PersonalController(FabricRepository fabric) : base(fabric)
        {
        }
    }
}
